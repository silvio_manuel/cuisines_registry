package de.quandoo.recruitment.registry.model;

import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotSame;
import static org.junit.Assert.assertSame;

public class CustomerTest {
    /*
     * Testing strategy:
     * We check uniqueness of instances as well
     * as possibility of exposure of an internal representation
     *
     * partition inputs as follows:
     * 2 customers with same UUID
     * 2 customers with different UUID
     * 1 customer with 2 cuisine -> mutate list of cuisines
     */

    @Before
    public void setUp() {
        Customer.clear();
    }

    @Test
    public void testSameInstances() {

        assertSame(Customer.getInstance("007"), Customer.getInstance("007"));

    }

    @Test
    public void testDifferentInstances() {

        assertNotSame(Customer.getInstance("007"), Customer.getInstance("006"));
    }

    @Test
    public void testNoInternalExposure() {
        Customer customer = Customer.getInstance("Pele");
        customer.addCuisine(Cuisine.ITALIAN);
        customer.addCuisine(Cuisine.GERMAN);
        List<Cuisine> cuisines = customer.getCuisines();

        assertEquals("Pele", customer.getUuid());
        assertEquals(2, cuisines.size());

        cuisines.remove(Cuisine.ITALIAN);
        assertEquals(1, cuisines.size());
        assertEquals(2, customer.getCuisines().size());
    }
}
