package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;
import org.junit.Before;
import org.junit.Test;

import java.util.List;

import static org.junit.Assert.*;

public class InMemoryCuisinesRegistryTest {

    /*
     * Testing strategy:
     * We will use different set of [Customer]s & [Cuisine]s to cover boundary conditions
     *
     * partition inputs as follow:
     *    Cuisine | Customer | Duplication | Top N
     * 1.    0         0           -           10
     * 2.    0         0           -           0
     * 3.    1         1           -           2
     * 4.    5         1        Cuisine        4
     * 5.    5         10    Customer/Cuisine  3
     */

    @Before
    public void cleanUp() {
         Cuisine.GERMAN.clear();
         Cuisine.FRENCH.clear();
         Cuisine.ITALIAN.clear();
         Customer.clear();
    }

    @Test
    public void testEmptyData() {
        InMemoryCuisinesRegistry registry = new InMemoryCuisinesRegistry();

        assertEquals(0, registry.topCuisines(10).size());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testWrongTopN() {
        InMemoryCuisinesRegistry registry = new InMemoryCuisinesRegistry();

        registry.topCuisines(0);
    }

    @Test
    public void testSingleData() {
        InMemoryCuisinesRegistry registry = new InMemoryCuisinesRegistry();
        Customer customer = Customer.getInstance("Christian");
        registry.register(customer, Cuisine.GERMAN);

        assertEquals(1, customer.getCuisines().size());
        assertEquals(Cuisine.GERMAN, customer.getCuisines().get(0));
        assertEquals(1, Cuisine.GERMAN.getCustomers().size());
        assertEquals(customer, Cuisine.GERMAN.getCustomers().get(0));

        List<Cuisine> top = registry.topCuisines(2);
        assertEquals(1, top.size());
        assertEquals(Cuisine.GERMAN, top.get(0));
    }

    @Test
    public void testDuplicationCuisine() {
        InMemoryCuisinesRegistry registry = new InMemoryCuisinesRegistry();
        Customer customer = Customer.getInstance("Christian");
        registry.register(customer, Cuisine.GERMAN);
        registry.register(customer, Cuisine.GERMAN);
        registry.register(customer, Cuisine.FRENCH);
        registry.register(customer, Cuisine.FRENCH);
        registry.register(customer, Cuisine.ITALIAN);

        assertEquals(3, customer.getCuisines().size());
        assertEquals(1, Cuisine.GERMAN.getCustomers().size());
        assertEquals(1, Cuisine.FRENCH.getCustomers().size());
        assertEquals(1, Cuisine.ITALIAN.getCustomers().size());
        assertEquals(3, registry.topCuisines(4).size());
    }

    @Test
    public void testDuplicationDataAndSortingOrder() {
        InMemoryCuisinesRegistry registry = new InMemoryCuisinesRegistry();
        Customer customer1 = Customer.getInstance("1");
        Customer customer2 = Customer.getInstance("2");
        Customer customer3 = Customer.getInstance("3");
        Customer customer4 = Customer.getInstance("4");

        registry.register(customer1, Cuisine.GERMAN);
        registry.register(customer2, Cuisine.GERMAN);
        registry.register(customer3, Cuisine.GERMAN);
        registry.register(customer4, Cuisine.GERMAN);

        registry.register(customer3, Cuisine.FRENCH);
        registry.register(customer3, Cuisine.FRENCH);
        registry.register(customer4, Cuisine.FRENCH);
        registry.register(customer4, Cuisine.FRENCH);

        registry.register(customer1, Cuisine.ITALIAN);
        registry.register(customer2, Cuisine.ITALIAN);
        registry.register(customer4, Cuisine.ITALIAN);
        registry.register(customer4, Cuisine.ITALIAN);

        assertEquals(2, customer1.getCuisines().size());
        assertEquals(2, customer2.getCuisines().size());
        assertEquals(2, customer3.getCuisines().size());
        assertEquals(3, customer4.getCuisines().size());

        List<Cuisine> top = registry.topCuisines(3);

        assertEquals(3, top.size());
        // test rightness of sorting order
        assertEquals(Cuisine.GERMAN, top.get(0));
        assertEquals(Cuisine.ITALIAN, top.get(1));
        assertEquals(Cuisine.FRENCH, top.get(2));
    }
}