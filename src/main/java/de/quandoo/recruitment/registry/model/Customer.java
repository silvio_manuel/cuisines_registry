package de.quandoo.recruitment.registry.model;

import java.util.*;

/**
 * Class is not threadsafe
 * An internal representation is mutable, so no reason to override hashCode and equals.
 * In the other case, class fully identified by UUID therefore we have to ensure that
 * there were not instances with the same UUID by caching ones and managing instance creation
 */
public class Customer {

    private final String uuid;

    /**
     * We use set to prevent duplication of data
     */
    private final Set<Cuisine> cuisines;

    /**
     * Storage for Customers
     */
    private static final Map<String, Customer> customers = new HashMap<>();

    /**
     * Constructor is private to manage creation of instances via dedicated method
     * @param uuid unique identifier
     */
    private Customer(final String uuid) {
        this.uuid = uuid;
        this.cuisines = new HashSet<>();
    }

    public void addCuisine(Cuisine cuisine) {
        this.cuisines.add(cuisine);
    }

    /**
     * @return copy of origin data to prevent external exposure
     */
    public List<Cuisine> getCuisines() {
        return new LinkedList<>(this.cuisines);
    }

    public String getUuid() {
        return uuid;
    }

    public static Customer getInstance(String uuid) {

        Customer instance = customers.get(uuid);

        if (instance == null)  {

            instance = new Customer(uuid);
            customers.put(uuid, instance);
        }

        return instance;
    }

    public static void clear() {

        customers.clear();
    }
}
