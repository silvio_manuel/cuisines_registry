package de.quandoo.recruitment.registry.model;

import java.util.*;

public enum Cuisine {

    GERMAN,
    FRENCH,
    ITALIAN;

    /**
     * Use set to prevent duplication
     */
    private final Set<Customer> customers;

    Cuisine() {
        this.customers = new HashSet<>();
    }

    /**
     * @param customer which likes this cuisine
     */
    public void addCustomer(Customer customer) {
        this.customers.add(customer);
    }

    /**
     * @return copy of origin data to prevent external exposure
     */
    public List<Customer> getCustomers() {
        return new LinkedList<>(customers);
    }

    public void clear() {
        this.customers.clear();
    }
}
