package de.quandoo.recruitment.registry;

import de.quandoo.recruitment.registry.api.CuisinesRegistry;
import de.quandoo.recruitment.registry.model.Cuisine;
import de.quandoo.recruitment.registry.model.Customer;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

/**
 * Class is threadsafe, no internal representation here.
 */
public class InMemoryCuisinesRegistry implements CuisinesRegistry {

    @Override
    public void register(final Customer userId, final Cuisine cuisine) {

        userId.addCuisine(cuisine);
        cuisine.addCustomer(userId);
    }


    @Override
    public List<Customer> cuisineCustomers(final Cuisine cuisine) {

        return cuisine.getCustomers();
    }

    @Override
    public List<Cuisine> customerCuisines(final Customer customer) {

        return customer.getCuisines();
    }

    @Override
    public List<Cuisine> topCuisines(final int n) {

        if (n < 1) throw new IllegalArgumentException("You have to specify number >= 1");

        return Arrays.stream(Cuisine.values())
                .filter(o -> o.getCustomers().size() > 0)
                .sorted(Comparator.comparingInt(o -> -o.getCustomers().size()))
                .limit(n)
                .collect(Collectors.toCollection(ArrayList::new));
    }
}
